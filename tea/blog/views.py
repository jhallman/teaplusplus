from django.http import HttpResponseRedirect, HttpResponse
from django.template import RequestContext
from django.template.loader import get_template
from django.shortcuts import render_to_response, get_object_or_404
from models import Blog_Entry
from forms import BlogForm, ContactForm
import re, string
from django.core.context_processors import csrf

def index(request):
	return render_to_response('index.html', {'blog': Blog_Entry.objects.order_by("-posted")[:5]})

def view_blog_post(request, slug):
	t = get_template('view_post.html')
	html = t.render(RequestContext(request, {'post': get_object_or_404(Blog_Entry, slug=slug)}))
	return HttpResponse(html)

def create_blog_post(request):
	if request.method == 'POST':
		form = BlogForm(request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			handle_data(cd['title'], cd['body'], cd['tags'])
			return HttpResponseRedirect('/blog/create/success')
	else:
		form = BlogForm
	return render_to_response('blogform.html', {'form': form})

def success(request):
	return render_to_response('success.html')

def about(request):
	return render_to_response('about.html')

def contact(request):
	if request.method == 'POST':
		pass
	else:
		form = ContactForm
	return render_to_response('contact.html', {'form': form})

def handle_data(title, body, tags):
	# sanitize title string for use as url slug
	slug = re.sub(r'[_]+', '-', (re.sub(r'[\W]+', '', ('_'.join(title.split())))))
	b = Blog_Entry(title=title, slug=slug, body=body, tags=tags)
	b.save()


