from django import forms
from models import Blog_Entry

class BlogForm(forms.ModelForm):
	class Meta:
		model = Blog_Entry
		fields = ('title', 'body', 'tags')

class ContactForm(forms.Form):
	subject = forms.CharField(max_length=100)
	email = forms.EmailField(required=False)
	comment = forms.CharField(widget=forms.Textarea)

class MyCommentForm(forms.Form):
	name = forms.CharField(max_length=100)
	email = forms.EmailField(required=False)
	comment = forms.CharField(widget=forms.Textarea)