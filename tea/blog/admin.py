from django.contrib import admin
from models import Blog_Entry

class MyAdmin(admin.ModelAdmin):
	fields = ('title', 'body', 'tags', 'slug')

admin.site.register(Blog_Entry, MyAdmin)