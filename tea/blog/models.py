from django.db import models
from django.db.models import permalink
import re


# blog entry
class Blog_Entry(models.Model):
	title = models.CharField(max_length=100)
	slug = models.SlugField(max_length=100)
	posted = models.DateTimeField(auto_now_add=True)
	body = models.TextField()
	tags = models.CharField(max_length=100, blank=True)

	def get_snippet(self):
		limit = 300
		words = re.split(r"(?=(?<=[^\s])\s+)", self.body)
		if len(words) > limit:
			end = ' ...'
		else: end = ''
		s = words[0:limit]
		snippet = ' '.join(s)
		return snippet+end

	def __unicode__(self):
		return '%s' % self.title

	class Meta:
		verbose_name = 'Blog'
		verbose_name_plural = 'Blogs'

	@permalink
	def get_absolute_url(self):
		return ('view_blog_post', None, {'slug': self.slug})

