from django.conf.urls import patterns, include, url
from blog.views import create_blog_post, index, view_blog_post, about, contact, success

# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'tea.views.home', name='home'),
    # url(r'^tea/', include('tea.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    # comment app url
    (r'^comments/', include('django.contrib.comments.urls')),

    #home page, shows 5 most recent posts
    (r'^$|^blog/view/$|^blog/$', index),

    #about page
    (r'^about/$', about),

    #contact page
    (r'^contact/$', contact),

    #shows a specific entry's page
    url(r'^blog/view/(?P<slug>[^\.]+)', 
    	view_blog_post),

    #form for creating a new entry
    (r'^blog/create/$', create_blog_post),
    (r'^blog/create/success/$', success),
)
